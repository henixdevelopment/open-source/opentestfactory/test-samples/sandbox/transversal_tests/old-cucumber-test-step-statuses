# Old Cucumber Test Step Statuses



# Project Name

This repository contains the source code and Cucumber tests for "Old Cucumber Test Step Statuses". The tests demonstrate different test outcomes, including passed, failed, and not run statuses.

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Before running the tests, make sure you have Java and Maven installed. If you don't have these installed, please follow the instructions for your operating system to install Java SDK and Maven.

### Setting Up Cucumber

1. Install Java:

2. Install Maven:

3. Clone the repository and navigate to the project directory:
   ```bash
   git clone https://gitlab.com/henixdevelopment/open-source/opentestfactory/test-samples/tests-transverses/old-cucumber-test-step-statuses
   cd old-cucumber-test-step-statuses
   cd cucumber-test-statuses 
   ```
