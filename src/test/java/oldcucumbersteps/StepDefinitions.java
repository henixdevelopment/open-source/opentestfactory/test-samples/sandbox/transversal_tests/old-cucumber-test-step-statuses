package oldcucumbersteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

public class StepDefinitions {

    @Given("I run a step")
    public void i_run_a_step() {
        System.out.println("I'm the first step");
    }

    @When("I run a failed step")
    public void i_run_a_failed_step() {
        System.out.println("I'm a failed step");
        fail("This step is intentionally failed.");
    }

    @Then("My step is in success")
    public void my_step_is_in_success() {
        System.out.println("I'm the last step and I'm a success");
    }

    @Then("My step is skipped")
    public void my_step_is_skipped() {
        System.err.println("I'm the last step and I'm skipped");
    }
}
